# coding: utf8

""""""
import os
from yiwa.db import Sqlite3DB
from yiwa.settings import BASE_DIR

s3db = Sqlite3DB(os.path.join(BASE_DIR,
                              "apps",
                              "self_discipline",
                              "self_discipline.s3db"))
