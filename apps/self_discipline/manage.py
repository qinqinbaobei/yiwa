# coding: utf8

"""自律表格管理页面
二维码管理, 不支持语音指令.
"""

import datetime
from flask_restful import Resource, reqparse
from apps import api
from apps.self_discipline.db import s3db
from yiwa.upload import FileUploader, FileStorage
from apps.self_discipline.configs import ImageRelPath

parser = reqparse.RequestParser()


# 维护兑换记录
class ExchangeRecords(Resource):
    """兑换记录"""

    def get(self):
        """查看兑换记录"""
        sql = f"""select award, number, `time`
            from exchange_records 
            where last=0
            order by time desc;
        """
        data = s3db.select_one(sql)
        return data

    def post(self):
        """添加兑换记录, 兑换奖品"""
        parser.add_argument("award_id", type=int, location='form')
        args = parser.parse_args()
        award_id = args.get("award_id")
        sql = f"select * from awards where id={award_id} and got=0;"
        award = s3db.select_one(sql)
        if award:
            if self.exchange(award):
                return {"code": 1, "msg": "兑换成功"}
        return {"code": 0, "msg": "不可兑换或失败"}

    def exchange(self, award_dict):
        """兑换"""
        award_id = award_dict.get("id")
        award = award_dict.get("name")
        number = award_dict.get("score")
        time = datetime.datetime.now()
        # 插入一条兑换记录
        sql = f"""insert into exchange_records(last, award, number, `time`) 
            values(0, '{award}', '{number}', '{time}');"""
        # 余额减少
        sql = f"""{sql}
            update exchange_records set balance=balance-{number}
            where last=1;
        """
        # 获取奖品，更新兑换时间
        sql = f"""{sql}
            update awards set got=1, got_time='{time}' where id={award_id};
        """
        result = s3db.execute(sql)
        return result


api.add_resource(ExchangeRecords, "/self_discipline/exchange_records")


# 维护奖品
class Award(Resource):
    """奖品"""

    def get(self, award_id):
        """某项"""
        sql = f"select * from awards where id={award_id};"
        data = s3db.select_one(sql)
        return data

    def patch(self, award_id):
        """更新某项"""
        parser.add_argument("name", type=str, location='form')
        parser.add_argument("score", type=int, location='form')
        parser.add_argument("order", type=int, location='form')
        parser.add_argument("image_file", type=FileStorage, location='files')
        args = parser.parse_args()
        name = args.get("name")
        score = args.get("score")
        order = args.get("order")
        image_file = args.get("image_file")
        file_name = FileUploader().image(ImageRelPath, image_file)
        image_path = f"image/{file_name}"
        sql = f"""update awards 
            set name='{name}', score='{score}', order='{order}', image_path='{image_path}' 
            where id={award_id};
        """
        if s3db.execute(sql):
            return {"code": 1, "msg": True}
        return {"code": 0, "msg": False}

    def delete(self, award_id):
        """逻辑删除某项"""
        sql = f"update awards set enable=0 where id={award_id};"
        if s3db.execute(sql):
            return {"code": 1, "msg": True}
        return {"code": 0, "msg": False}


class Awards(Resource):
    """奖品列表"""

    def get(self):
        """返回未领取的奖品"""
        sql = f"select * from awards where got=0;"
        data = s3db.select(sql, as_dict=True)
        return data

    def post(self):
        """添加奖品"""
        parser.add_argument("name", type=str, location='form')
        parser.add_argument("score", type=int, location='form')
        parser.add_argument("order", type=int, location='form')
        parser.add_argument("image_file", type=FileStorage, location='files')
        args = parser.parse_args()
        name = args.get("name")
        score = args.get("score")
        order = args.get("order")
        image_file = args.get("image_file")
        file_name = FileUploader().image(ImageRelPath, image_file)
        image_path = f"image/{file_name}"
        sql = f"""insert into awards(name, score, order, image_path) 
            values('{name}', '{score}', '{order}', '{image_path}');"""
        last_id = s3db.execute(sql, result_set=True)
        if last_id:
            return {"code": 1, "msg": last_id}
        return {"code": 0, "msg": 0}


api.add_resource(Award, "/self_discipline/awards/<int:award_id>")
api.add_resource(Awards, "/self_discipline/awards")


# 维护分值鼓励项
class Score(Resource):
    """分值项"""

    def get(self, score_id):
        """某个分值项"""
        sql = f"select * from scores where id={score_id};"
        data = s3db.select_one(sql)
        return data

    def patch(self, score_id):
        """更新某项"""
        parser.add_argument("name", type=str, location='form')
        parser.add_argument("image_file", type=FileStorage, location='files')
        args = parser.parse_args()
        name = args.get("name")
        image_file = args.get("image_file")
        file_name = FileUploader().image(ImageRelPath, image_file)
        image_path = f"image/{file_name}"
        sql = f"""update scores 
            set name='{name}', image_path='{image_path}' 
            where id={score_id};
        """
        if s3db.execute(sql):
            return {"code": 1, "msg": True}
        return {"code": 0, "msg": False}

    def delete(self, score_id):
        """逻辑删除某项"""
        sql = f"update scores set enable=0 where id={score_id};"
        if s3db.execute(sql):
            return {"code": 1, "msg": True}
        return {"code": 0, "msg": False}


class Scores(Resource):
    """分值列表"""

    def get(self):
        """返回列表"""
        sql = f"select * from scores where enable=1;"
        data = s3db.select(sql, as_dict=True)
        return data

    def post(self):
        """添加分值项"""
        parser.add_argument("name", type=str, location='form')
        parser.add_argument("image_file", type=FileStorage, location='files')
        args = parser.parse_args()
        name = args.get("name")
        image_file = args.get("image_file")
        file_name = FileUploader().image(ImageRelPath, image_file)
        image_path = f"image/{file_name}"
        # 所有分值都是1
        sql = f"""insert into scores(score, name, image_path) 
            values(1, '{name}', '{image_path}');"""
        last_id = s3db.execute(sql, result_set=True)
        if last_id:
            return {"code": 1, "msg": last_id}
        return {"code": 0, "msg": 0}


api.add_resource(Score, "/self_discipline/scores/<int:score_id>")
api.add_resource(Scores, "/self_discipline/scores")


# 维护每日计分项
class Options(Resource):
    """每日计分项列表"""

    def get(self):
        """返回列表"""
        sql = f"select * from options where enable=1;"
        data = s3db.select(sql, as_dict=True)
        return data

    def post(self):
        """添加计分项"""
        parser.add_argument("name", type=str, location='form')
        parser.add_argument("order", type=int, location='form')
        parser.add_argument("image_file", type=FileStorage, location='files')
        args = parser.parse_args()
        name = args.get("name")
        order = args.get("order")
        image_file = args.get("image_file")
        file_name = FileUploader().image(ImageRelPath, image_file)
        image_path = f"image/{file_name}"
        sql = f"""insert into options(name, order, image_path) 
            values('{name}', '{order}', '{image_path}');"""
        last_id = s3db.execute(sql, result_set=True)
        if last_id:
            return {"code": 1, "msg": last_id}
        return {"code": 0, "msg": 0}


class Option(Resource):
    """每日计分项"""

    def get(self, option_id):
        """某项"""
        sql = f"select * from options where id={option_id};"
        data = s3db.select_one(sql)
        return data

    def patch(self, option_id):
        """更新某项"""
        parser.add_argument("name", type=str, location='form')
        parser.add_argument("order", type=int, location='form')
        parser.add_argument("image_file", type=FileStorage, location='files')
        args = parser.parse_args()
        name = args.get("name")
        order = args.get("order")
        image_file = args.get("image_file")
        file_name = FileUploader().image(ImageRelPath, image_file)
        image_path = f"image/{file_name}"
        sql = f"""update options 
            set name='{name}', order='{order}', image_path='{image_path}' 
            where id={option_id};
        """
        if s3db.execute(sql):
            return {"code": 1, "msg": True}
        return {"code": 0, "msg": False}

    def delete(self, option_id):
        """逻辑删除某项"""
        sql = f"update options set enable=0 where id={option_id};"
        if s3db.execute(sql):
            return {"code": 1, "msg": True}
        return {"code": 0, "msg": False}


api.add_resource(Option, "/self_discipline/options/<int:option_id>")
api.add_resource(Options, "/self_discipline/options")
