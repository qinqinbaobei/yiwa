# coding: utf8

"""yaml配置操作"""
import os
from ruamel.yaml import round_trip_dump
from ruamel.yaml.util import load_yaml_guess_indent

# yiwa根目录
YAML_FILE = os.path.join(os.path.dirname(os.path.abspath(__file__)), "yiwa.yaml")


def path_parser(path):
    """解析yaml配置参数路径"""
    separators = ("/", ".", ",", "\\", ":", "|")
    separator = " "
    for sep in separators:
        if sep in path:
            separator = sep
            break
    return path.split(separator)

def get_value_by_path(*key_path, default):
    """根据路径获得配置"""
    if not key_path:
        return None

    if 1 == len(key_path):
        key_path = path_parser(key_path[0])

    # 参考https://codeday.me/bug/20180824/228351.html
    config, ind, bsi = load_yaml_guess_indent(open(YAML_FILE))
    layer = len(key_path)
    for index, key in enumerate(key_path, 1):
        if index == layer:
            return config.get(key, default)
        config = config.get(key)


def set_value_by_path(*key_path, value):
    """根据路径更新配置"""
    if not key_path:
        return None

    if 1 == len(key_path):
        key_path = path_parser(key_path[0])

    try:
        # 参考https://codeday.me/bug/20180824/228351.html
        config, ind, bsi = load_yaml_guess_indent(open(YAML_FILE))
        layer = len(key_path)
        temp_config = config    # 利用浅拷贝，地址引用的技巧更新config
        for index, key in enumerate(key_path, 1):
            if index == layer:
                temp_config.update({key: value})
            temp_config = temp_config[key]
        round_trip_dump(config, open(YAML_FILE, 'w'),
                        indent=ind, block_seq_indent=bsi)
        return True
    except:
        return False


if __name__ == "__main__":
    print(get_value_by_path("yiwa", "listening", "interval_time", default=2))
    print(get_value_by_path("yiwa.listening.interval_time", default=2))
    print(get_value_by_path("", default=""))
    set_value_by_path("yiwa", "listening", "interval_time", value=1)